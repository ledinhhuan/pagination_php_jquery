<?php
    include "config.php";
    class Pagination {
        private  $set_record_limit = 4;

        public function connectionDatabase() {
            $db_host = DB_SERVER;
            $db_name = DB_DATABASE;
            $db_user = DB_USERNAME;
            $db_pass = DB_PASSWORD;

            try {
                $dbConnection = new PDO("mysql:host=$db_host;port=3305;dbname=$db_name", $db_user, $db_pass);
                $dbConnection->exec("set names utf8");
                $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                return $dbConnection;
            }
            catch (PDOException $e) {
                echo 'Connection failed: ' . $e->getMessage();
            }
        }

        public function showData($page_number) {
            $set_offset_limmit = $page_number * $this->set_record_limit;

            try {
                $db_connection = $this->connectionDatabase();

                $stmt = $db_connection->prepare("select * from products LIMIT :record_limit OFFSET :set_offset_limit");
                $stmt->bindParam(':record_limit', $this->set_record_limit, PDO::PARAM_INT);
                $stmt->bindParam(':set_offset_limit',$set_offset_limmit, PDO::PARAM_INT);
                $stmt->execute();

                $count = $stmt->rowCount();
                if($count > 0) {
                    while($data = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        echo '<div class="card mb-4"> <div class="card-header">'.$data['id'].') '.$data['productname'].' </div> <div class="card-body"> <blockquote class="blockquote mb-0"> <p>'.$data['content'].'</p> <footer class="blockquote-footer">Someone famous in <cite title="Source Title">Source Title</cite></footer> </blockquote> </div> </div>';
                    }
                }
            } catch (PDOException $e) {
                echo 'Show data failed: ' . $e->getMessage();
            }
        }

        public function getTotalNumberOfRecord() {
            try {
                $db_connection = $this->connectionDatabase();
                $stmt = $db_connection->prepare("select * from products");
                $stmt->execute();
                $count = $stmt->rowCount();
                if($count > 0) {
                    echo $total_pages = ceil($count/$this->set_record_limit);
                } else {
                    echo "0";
                }
            } catch (PDOException $e) {
                echo "Get total error: " . $e->getMessage();
            }
        }
    }
?>