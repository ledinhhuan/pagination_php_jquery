<?php
    include 'pagination.php';
    if(isset($_POST['pageNum'])) {
        $page_number = filter_var($_POST['pageNum'], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);
        $pagination = new Pagination();
        echo $pagination->showData($page_number);
    }
?>