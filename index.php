<?php include 'process_pagination.php'; ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Create Pagination Ajax and PHP</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <h3 class="display-4 text-center">Pagination PHP and Ajax</h3>
        <div class="row justify-content-center" id="load-data">
        <!-- content here-->
        </div>
        <div class="row justify-content-center">
            <span id="loading"><img src="loading.gif" alt="Loading"></span>
        </div>
        <div class="row justify-content-center mt-4">
            <nav aria-label="Page navigation example">
                <ul class=" justify-content-center pagination-sm" id="pagination-demo">

                </ul>
            </nav>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twbs-pagination/1.4.1/jquery.twbsPagination.min.js"></script>
    <script>

        $(document).ready(function()  {

            $('#pagination-demo').twbsPagination({
                totalPages: <?php $page = new Pagination();  $page->getTotalNumberOfRecord(); ?>,
                visiblePages: 4,
                next: 'Next',
                prev: 'Prev',
                onPageClick: function (event, page) {
                    loadData(page-1);
                }
            });

            function loadData(pageNum) {
                dataString = "pageNum="+pageNum;
                $.ajax({
                    type: 'POST',
                    url: 'process_pagination.php',
                    data: dataString,
                    cache: false,
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    success: function(html){
                        $( "#load-data" ).html(  html );
                        $( "#loading" ).hide();
                    }
                });
            }
            loadData(0);
        });
    </script>
</body>
</html>